from django import forms

from .models import Action


class ActionForm(forms.ModelForm):
    """
    A form to create or edit an Action
    """
    description = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = Action
        fields = ('title', 'description', 'assigned_to')
