import factory
from factory.django import DjangoModelFactory


class ActionFactory(DjangoModelFactory):

    class Meta:
        model = 'actions.Action'

    title = factory.Sequence(lambda n: 'Title %d' % n)
    description = factory.Sequence(lambda n: 'Bill Address (1) %d' % n)
    assigned_to = factory.SubFactory('accounts.factories.UserFactory')
    created_by = factory.SubFactory('accounts.factories.UserFactory')
