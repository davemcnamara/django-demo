from django.db import models

from accounts.models import User


class Action(models.Model):
    """
    title: text field identifying the Action
    description: text field providing a description for the Action
    assigned_to: User to which this action is assigned

    created: auto-generated datetime when the Action was created
    created_by: User who created the Action
    """
    title = models.CharField(max_length=30)
    description = models.CharField(max_length=400, blank=True, null=True)
    assigned_to = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name="actions")

    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)

    def __unicode__(self):
        return self.title
