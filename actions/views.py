import django.utils.timezone as timezone

from django.core.urlresolvers import reverse_lazy
from django.views.generic import TemplateView, CreateView, UpdateView, DeleteView, DetailView

from datetime import date, timedelta
# import arrow
import calendar
from collections import OrderedDict

from accounts.mixins import LoginRequiredMixin
from .models import Action
from .forms import ActionForm


class ActionMonthView(LoginRequiredMixin, TemplateView):
    month = timezone.now().month
    year = timezone.now().year
    template_name = 'actions/action_month.html'

    def get_context_data(self, **kwargs):
        context = super(ActionMonthView, self).get_context_data(**kwargs)
        cal = calendar.Calendar(calendar.SUNDAY)
        results = OrderedDict()
        for day in cal.itermonthdates(self.year, self.month):
            tomorrow = day + timedelta(days=1)
            results[day] = Action.objects.filter(created__gte=day, created__lt=tomorrow)

        context.update({
            'now': timezone.now(),
            'results': results,
        })

        return context


class ActionCreateView(LoginRequiredMixin, CreateView):
    model = Action
    template_name = 'actions/action_form.html'
    form_class = ActionForm
    success_url = reverse_lazy('actions_current')


class ActionDetailView(LoginRequiredMixin, DetailView):
    model = Action
    template_name = 'actions/action_detail.html'


class ActionUpdateView(LoginRequiredMixin, UpdateView):
    model = Action
    template_name = 'actions/action_form.html'
    form_class = ActionForm
    success_url = reverse_lazy('actions_current')


class ActionDeleteView(LoginRequiredMixin, DeleteView):
    model = Action
    success_url = reverse_lazy('actions_current')
